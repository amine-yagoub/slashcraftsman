mongo -- "$MONGO_INITDB_DATABASE" <<EOF
  db.createUser({
     user: $(_js_escape "$MONGO_INITDB_ROOT_USERNAME"),
     pwd: $(_js_escape "$MONGO_INITDB_ROOT_PASSWORD"),
     roles: [ "readWrite", $(_js_escape "$MONGO_INITDB_DATABASE") ]
     })
EOF